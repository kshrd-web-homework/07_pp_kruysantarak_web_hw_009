import "bootstrap/dist/css/bootstrap.css";
import "./app.css";
import NavBar from "./components/NavBar";

import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Home from "./Pages/Home"
import Account from "./Pages/Account"
import Video from "./Pages/Video"
import Welcome from "./Pages/Welcome"
import Authentication from "./Pages/Authentication"
import ItemDetail from './Pages/ItemDetail'
import {ProtectedRoute} from './Pages/ProtectedRoute'

export default function App() {
  return (
    <div>
      <Router>
      <NavBar />
      <Container>
        <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/video" component={Video}/> 
        <Route path="/welcome" component={Welcome}/>
        <Route path="/account" component={Account}/>
        <ProtectedRoute exact path="/authentication" component={Authentication} />
        <Route path="/ItemDetail/:id/:path/:image/:title/:desc" component={ItemDetail}/>
        </Switch>
      </Container>
      </Router>
    </div>
  );
}