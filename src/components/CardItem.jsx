import React, {useState} from "react";
import {Link} from 'react-router-dom'
import '../app.css';

export default function CardItem(props) {

  const [items,SetItems] = useState(props)

  return (
    <>
      <div className="row">
        {items.items.map((item, key) => (
                  <div className="col-lg-3 col-md-6 col-sm12">
        <div key={key} className="card mb-5" style={{width : '100%'}}>
        <img src={item.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{item.title}</h5>
          <p className="card-text">
            {item.desc}
          </p>
          <Link to={`/ItemDetail/${item.id}/${item.image}/${item.title}/${item.desc}`} className="btn btn-primary">
            Read
          </Link>
        </div>
      </div>
              </div>
      ))}
      </div>
    </>
    // <div className='d-flex flex-row justify-content-between flex-wrap'>
    //   {items.items.map((item, key) => (
    //     <div key={key} className="card mb-5" style={{width : '15rem'}}>
    //     <img src={item.image} className="card-img-top" alt="..." />
    //     <div className="card-body">
    //       <h5 className="card-title">{item.title}</h5>
    //       <p className="card-text">
    //         {item.desc}
    //       </p>
    //       <Link to={`/ItemDetail/${item.id}/${item.image}/${item.title}/${item.desc}`} className="btn btn-primary">
    //         Read
    //       </Link>
    //     </div>
    //   </div>
    //   ))}
    // </div>
  );
}
