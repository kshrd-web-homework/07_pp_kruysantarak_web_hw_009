import React, {useState} from "react";
import { Link, Route, Switch, useParams, useRouteMatch, useLocation } from "react-router-dom";

export default function Animation() {
  let fetchAnimations = [
    {
      id: 1,
      type: "Action",
    },
    {
      id: 2,
      type: "Romance",
    },
    {
      id: 3,
      type: "Comedy",
    },
  ];

  const [animations, setAnimations] = useState(fetchAnimations);

  let { path, url } = useRouteMatch();

  let query = new URLSearchParams(useLocation().search);

  return (
    <div>
      <h4 className="mb-3">Animation Category</h4>
      {animations.map((animation, key) => (
        <Link key={key} to={`${url}?type=${animation.type}`} className="btn btn-primary mb-3" style={{ marginRight: "4px" }}>
          {animation.type}
        </Link>
      ))}
        <AnimationType type={query.get("type")}/>
    </div>
  );
}

function AnimationType({type}) {
  console.log(type)
  return (
    <div>
    {type ? (
      <h5>Animation Type : <b className="text-primary">{type}</b></h5>
    ) : (
      <h5>Please Select Animation Category.</h5>
    )}
    </div>
  );
}
