import React, { useState } from "react";
import { Link, Route, Switch, useParams, useRouteMatch, useLocation } from "react-router-dom";

export default function Movie() {
  let fetchMovies = [
    {
      id: 1,
      type: 'Adventure'
    },
    {
      id: 2,
      type: 'Crime'
    },
    {
      id: 3,
      type: 'Action'
    },
    {
      id: 4,
      type: 'Romance'
    },
    {
      id: 5,
      type: 'Comedy'
    },
  ]
  const [movies, setMovies] = useState(fetchMovies)

  let { path, url } = useRouteMatch();
  console.log(url)

  let query = new URLSearchParams(useLocation().search);

  return (
    <div>
      <h4 className="mb-3">Movie Category</h4>
      {movies.map((movie, key) => (
        <Link key={key} to={`${url}?type=${movie.type}`} className="btn btn-primary mb-3" style={{ marginRight: '4px' }}>
          {movie.type}
        </Link>
      ))}
      <MovieType type={query.get("type")} />
    </div>
  );
}

function MovieType({ type }) {
  return (
    <div>
      {type ? (
        <h5>Movie Type : <b className="text-primary">{type}</b></h5>
      ) : (
        <h5>Please Select Movie Category.</h5>
      )}

    </div>
  );
}