import React from 'react'
import auth from './auth'

export default function Authentication(props) {
  return (
    <div>
      <h1>Authentication pages </h1>
      <button
        onClick={() => {
          auth.logout(() => {
            props.history.push("/welcome");
          });
        }}
        type="button"
        class="btn btn-primary btn-lg"
      >
        Logout
      </button>
    </div>
  )
}
