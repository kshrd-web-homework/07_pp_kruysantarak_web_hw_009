import React from "react";
import auth from "./auth";

export default function Welcome(props) {
  return (
    <div>
      <button
        onClick={() => {
          auth.login(() => {
            props.history.push("/authentication");
          });
        }}
        type="button"
        class="btn btn-primary btn-lg"
      >
        Login
      </button>
    </div>
  );
}
