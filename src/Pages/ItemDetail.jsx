import React from 'react'
import {Link} from 'react-router-dom'

export default function ItemDetail({match}) {

  const {id, path, image, title, desc} = match.params;

  let imagePath = '/'+path+'/'+image;

  return (
    <div>
        <div className="card mb-5" style={{width : '25rem'}}>
        <img src={imagePath} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">
            {desc}
          </p>
          <Link to="/" className="btn btn-primary">
            Back
          </Link>
        </div>
      </div>
    </div>
  )
}
