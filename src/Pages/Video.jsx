import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Movie from "./Movie";
import Animation from "./Animation";

export default function Video() {
  return (
    <div>
      <>
        <div className="mb-4">
          <h1>video</h1>
          <Link to="/video/movie" className="btn btn-primary">
            Movie
          </Link>{" "}
          <Link to="/video/animation" className="btn btn-primary">
            Animation
          </Link>
        </div>
        <Switch>
        <Route path="/video/movie" component={Movie} />
        <Route path="/video/animation" component={Animation} />
        </Switch>
      </>
    </div>
  );
}
