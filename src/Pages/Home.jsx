import React, {useState, useEffect} from 'react'
import CardItem from '../components/CardItem'

export default function Home() {

  let fetchItems = [
    {
      id : 1,
      image : 'image/html.gif',
      title : 'HTML 5',
      desc : 'Hello From HTML5'
    },
    {
      id : 2,
      image : 'image/css3.gif',
      title : 'CSS 3',
      desc : 'Hello From CSS3'
    },
    {
      id : 3,
      image : 'image/js.gif',
      title : 'JavaScript',
      desc : 'Hello From JavaScript'
    },
    {
      id : 4,
      image : 'image/reactjs.gif',
      title : 'React Js',
      desc : 'Hello From React Js'
    },
    {
      id : 5, 
      image : 'image/nodejs.gif',
      title : 'Node Js',
      desc : 'Hello From Node Js'
    },
    {
      id : 6,
      image : 'image/springboot.gif',
      title : 'Spring Boot',
      desc : 'Hello From Spring Boot'
    },
  ]

  const [items, setItems] = useState(fetchItems)
  useEffect(() => {
    setItems(fetchItems)
  }, [])

  return (
    <div>
      <div className="cards">
      <CardItem
        items={items}
      />
      </div>
    </div>
  )
}
