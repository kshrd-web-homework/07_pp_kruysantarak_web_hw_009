import React, {useState} from "react";
import { BrowserRouter as Router, Link, Route, Switch, useParams, useRouteMatch } from "react-router-dom";

export default function Account() {
  let fetchAccounts = [
    {
      id : 1,
      name : 'Netflix'
    },
    {
      id : 2,
      name : 'Zillow Group'
    },
    {
      id : 3,
      name : 'Yahoo'
    },
    {
      id : 4,
      name : 'Modus Create'
    }
  ]
  const [accounts, setAccounts] = useState(fetchAccounts)

  let { path, url } = useRouteMatch();

  return (
    <div>
      <>
      <h4 className="mb-3">Movie Category</h4>
      {accounts.map((account, key) => (
        <Link key={key} to={`${url}/${account.name}`} className="btn btn-primary mb-3" style={{marginRight: '4px'}}>
        {account.name}
      </Link>
      ))}
      <Switch>
        <Route path={`${path}/:name`}>
          <AccountName />
        </Route>
      </Switch>
      </>
    </div>
  );
}

function AccountName() {
  let { name } = useParams();
  return (
    <div>
      <h5>Account Type : <b className="text-primary">{name}</b></h5>
    </div>
  );
}